# Flask-skeleton

### Example:

```sh 
$ cd flask-skeleton 
$ python -m venv venv/ 
$ source venv/bin/activation 
$ pip install -r requirements.txt 
$ export FLASK_APP=main.py 
$ flask run --host=0.0.0.0 (listen all ips) 
```
